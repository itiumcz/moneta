#Compilation
After cloning repository run:

`mvn clean install` 

for compilation of project. It will run also very minimalistic set of JUnit test to check if there are any problems with encoding

#Usage
##Server start
You can run app by one of following approaches:

deploy final war to external Tomcat server (or server of your choice)

run Start class as usual Java program. Embedded Tomcat will be started for you
 
##Sending message
Use REST client of your choice to POST JSON message in format:

```
{
"text": "Příliš žluťoučký kůň úpěl ďábelské ódy"
}
```
