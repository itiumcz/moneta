package cz.itium.moneta.demo.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("cz.itium.moneta.demo.service")
public class ServiceConfig {
}
