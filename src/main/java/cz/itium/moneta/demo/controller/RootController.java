package cz.itium.moneta.demo.controller;

import cz.itium.moneta.demo.dto.Message;
import cz.itium.moneta.demo.service.IMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RootController {

    final static Logger logger = LoggerFactory.getLogger(RootController.class);

    @Autowired
    IMessageService messageService;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Message root(@RequestBody Message message) {
        logger.info("Root method called");
        return messageService.transformMessage(message);
    }

}
