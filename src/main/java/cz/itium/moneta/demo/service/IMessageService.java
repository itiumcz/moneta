package cz.itium.moneta.demo.service;

import cz.itium.moneta.demo.dto.Message;

public interface IMessageService {

    Message transformMessage(Message message);
}
