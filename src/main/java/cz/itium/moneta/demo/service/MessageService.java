package cz.itium.moneta.demo.service;

import cz.itium.moneta.demo.dto.Message;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class MessageService implements IMessageService {

    final static Logger logger = LoggerFactory.getLogger(MessageService.class);

    //TODO: may be extend replacement chars by other czech options as needed
    private static final String SEARCH_CHARS  = "aáeéiíoóuú";
    private static final String REPLACE_CHARS = "AÁEÉIÍOÓUÚ";

    @Override
    public Message transformMessage(Message message) {
        logger.info("Entering transformMessage {}", message.getText());
        String reverted = StringUtils.reverse(message.getText().toLowerCase());
        String replaced = StringUtils.replaceChars(reverted, SEARCH_CHARS, REPLACE_CHARS);
        String whiteSpaced = replaced.replaceAll("\\s+"," ");
        Message toReturn = new Message();
        toReturn.setText(whiteSpaced);
        logger.info("Leaving transformMessage {}", toReturn.getText());
        return toReturn;
    }
}
