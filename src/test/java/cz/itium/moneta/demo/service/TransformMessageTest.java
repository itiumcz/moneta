package cz.itium.moneta.demo.service;

import cz.itium.moneta.demo.config.ServiceConfig;
import cz.itium.moneta.demo.dto.Message;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ServiceConfig.class})
public class TransformMessageTest {

    private static final String ENCODING_FROM = "Příliš žluťoučký  kůň úpěl      ďábelské    ódy";
    private static final String ENCODING_TO = "ydÓ ÉkslEbÁď lěpÚ ňůk ýkčUOťUlž šIlÍřp";
    private static final String EXAMPLE1_FROM = "Ahoj, jak se máš?";
    private static final String EXAMPLE1_TO = "?šÁm Es kAj ,jOhA";
    private static final String EXAMPLE2_FROM = "Je mi fajn.";
    private static final String EXAMPLE2_TO = ".njAf Im Ej";

    @Autowired
    IMessageService messageService;

    @Test
    public void encodingTest() {
        testPair(ENCODING_FROM, ENCODING_TO);
    }

    @Test
    public void example1Test() {
        testPair(EXAMPLE1_FROM, EXAMPLE1_TO);
    }

    @Test
    public void example2Test() {
        testPair(EXAMPLE2_FROM, EXAMPLE2_TO);
    }

    private void testPair(String from, String to) {
        Message toTest = new Message();
        toTest.setText(from);
        Message transformed = messageService.transformMessage(toTest);
        Assert.assertEquals(to, transformed.getText());
    }
}
